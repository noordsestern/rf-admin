*** Settings ***
Resource     resources/os_demo.resource

*** Tasks ***
Identify operating system
    ${os}   get os
    Log     ${os}